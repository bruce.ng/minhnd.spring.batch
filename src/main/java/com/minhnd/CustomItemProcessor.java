package com.minhnd;

import com.minhnd.model.Report;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by Administrator on 10/6/2015.
 */
public class CustomItemProcessor implements ItemProcessor<Report, Report> {

    @Override
    public Report process(Report item) throws Exception {

        System.out.println("Processing..." + item);
        return item;
    }

}
